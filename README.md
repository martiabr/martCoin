# Project Brown

## Ideas:

Start by mining coins on your computer by clicking (wait time to process block)

Then build computers that mine for you, also solar panels to power the computers

The price rises exponentially as you buy more, need to find a balanced multiplier

Should add cool mining in progress animation inspired by the real deal, something with processing huge numbers and
hashes and shit

Should get the ability to transfer to different currencies?

Should be able to invest in the stock market, need to build a model to simulate the stock price, using geometric
brownian motion (GBM)

Should also be able to invest in mining factories after a while, upgrade CPU power and memory, also super computer /
quantum computer?

Also create botnets?
Could use them to spread malware that uses other computers to mine for you mwuhahaha
Should be able to balance how much of your workforce thats mining or spreading malware, i.e. either generating CPUs or generating coins.

Should have some other currency that is used for upgrades, dollars to buy hardware, but maybe use botnet or something
to get other kind of currency like influence?

Should also have something with encryption?

you first have to manually sell coins, you then buy a upgrade that sells for you, you can control how fast you sell, but faster -> less money per coin
should be able to upgrade coin price. Should then either remove manual selling or have a fixed manual selling rate.

build AI that improves stock trading, should maybe introduce r&d points that you get from running your workforce in research mode instead of mining or malware mode
use dollars and r&d points to upgrade AIs hardware and software

need a big data structure to handle upgrades and their triggers and permissions etc
also need to work more on terminal with stock stats, mining stats, upgrade confirmation etc

Upgrade algorithm for more efficient mining or upgrade instruction set
Upgrade market value
Upgrades to CPUs: more cores, more threads, upgrade clock, boost clock??
Also upgrade difficulty to encryption, more time but even more coins.
Upgrade stock market stakes in order to win faster

You buy AI threads that you can balance on different tasks. First you can only do R&D i.e. generating research points.
Then you can use them for scanning for new nodes i.e. increasing the available memory to be hacked,
also actually deploy the malware script i.e. increase the usable memory.
Finally you can use threads for actually mining

Item that improves chance?

## To do:

- [ ] Fix autoseller
- [x] Toggle button
- [x] Scaling of numbers (million/billion dollars, kJ -> MJ -> GJ etc.)
- [ ] Develop design
- [x] Upgrades and unlocks system
- [x] Add mining factories and super computers
- [x] Add the botnet
- [x] Add AI system for R&D
- [ ] Work on balance
- [x] Saving
- [ ] Run in background
- [ ] Iterate jQuery updates for more maintainable code
- [ ] Points when operation runs smoothly i.e. enough energy?
- [x] Change upgrade buttons with divs with more information and custom design
- [ ] Grid in stock animation

## Sources:

https://www.wikiwand.com/en/Geometric_Brownian_motion
https://www.wikiwand.com/en/Botnet
https://www.wikiwand.com/en/SHA-2

## Upgrades:

0.  Upgrade CPUs
1.  Install autoMiner script (unlock autoMiner)
2.  Increase martCoin market value percentage
3.  Upgrade CPUs 2 (prereq 0)
4.  Upgrade solar panels
5.  Upgrade mining algorithm (faster mining)
6.  Increase martCoin market value percentage 2 (prereq 2)
7.  Install autoSeller script (unlock autoSeller)
8.  Upgrade encryption difficulty (more mining time, but even more coins)
9.  Upgrade CPUs 3 (prereq 3)
10.  Unlock superComputers
11.  Increase martCoin market value percentage 3 (prereq 6)
12.  Unlock reactor
13.  Upgrade encryption difficulty 2 (prereq 8)
14.  Upgrade superComputers (prereq 10)



-------------------


![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Example [Jekyll] website using GitLab Pages.  View it live at https://pages.gitlab.io/jekyll

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Getting Started](#getting-started)
  - [Start by forking this repository](#start-by-forking-this-repository)
  - [Start from a local Jekyll project](#start-from-a-local-jekyll-project)
- [GitLab CI](#gitlab-ci)
- [Using Jekyll locally](#using-jekyll-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Other examples](#other-examples)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started

You can get started with GitLab Pages using Jekyll easily by either forking this repository or by uploading a new/existing Jekyll project.

Remember you need to wait for your site to build before you will be able to see your changes.  You can track the build on the **Pipelines** tab.

### Start by forking this repository

1. Fork this repository.
1. **IMPORTANT:** Remove the fork relationship.
Go to **Settings (⚙)** > **Edit Project** and click the **"Remove fork relationship"** button.
1. Enable Shared Runners.
Go to **Settings (⚙)** > **Pipelines** and click the **"Enable shared Runners"** button.
1. Rename the repository to match the name you want for your site.
1. Edit your website through GitLab or clone the repository and push your changes.

### Start from a local Jekyll project

1. [Install][] Jekyll.
1. Use `jekyll new` to create a new Jekyll Project.
1. Add [this `.gitlab-ci.yml`](.gitlab-ci.yml) to the root of your project.
1. Push your repository and changes to GitLab.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: ruby:2.3

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Other examples

* [jekyll-branched](https://gitlab.com/pages/jekyll-branched) demonstrates how you can keep your GitLab Pages site in one branch and your project's source code in another.
* The [jekyll-themes](https://gitlab.com/groups/jekyll-themes) group contains a collection of example projects you can fork (like this one) having different visual styles.

## Troubleshooting

1. CSS is missing! That means two things:
    * Either that you have wrongly set up the CSS URL in your templates, or
    * your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
