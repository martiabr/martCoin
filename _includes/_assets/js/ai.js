class AI {
    constructor () {
        this.totalThreads = 5;
        this.totalFreeThreads = 4;
        this.threads = [0, 0, 0, 1]; // 0: research, 1: scan, 2: hacking, 3: mining

        this.memory = 0;
        this.availableMemory = 0;
    }

    addThread (id) {
        if (this.totalFreeThreads && id >= 0 && id < this.threads.length) {
            this.totalFreeThreads --;
            this.threads[id] ++;
        }
    }

    removeThread (id) {
        if (id >= 0 && id < this.threads.length && this.threads[id]) {
            this.totalFreeThreads ++;
            this.threads[id] --;
        }
    }
}