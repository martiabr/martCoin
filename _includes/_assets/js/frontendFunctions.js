function updateElements (player) {
    // Resources:
    $('#money').text('Money: $' + suffixAndRound(player.money, 2));
    $('#coins').text('MartCoins: ' + suffixAndRound(player.coins, 2));
    $('#energy').text('Energy: ' + suffixAndRound(player.energy, 2) + 'J / '
        + suffixAndRound(player.batteries.capacity * player.batteries.total, 2) + 'J');
    $('#researchPoints').text('ResearchPoints: ' + suffixAndRound(player.researchPoints, 2));

    // Resources per second:
    var plus = '';
    if (player.energyPerSecond >= 0) {plus = '+';}
    $('#energyPerSecond').text(' (' + plus + suffixAndRound(player.energyPerSecond, 2) + 'J/s)');

    plus = '';
    if (player.coinsPerSecond >= 0) {plus = '+';}
    $('#coinsPerSecond').text(' (' + plus + suffixAndRound(player.coinsPerSecond, 2) + '/s avg.)');

    $('#moneyPerSecond').text(' (+$' + suffixAndRound(player.moneyPerSecond, 2) + '/s)');

    $('#RPsPerSecond').text(' (+' + suffixAndRound(player.RPsPerSecond, 2) + '/s)');

    // Items:
    $('#buyCPU').html('Buy CPU (' + player.consumers.miners.total + ')');
    $('#CPUCost').text('Cost: $' + suffixAndRound(player.consumers.miners.price, 2));

    if (!$('#buyCPU').prop('disabled') && player.money < player.consumers.miners.price) {
        $('#buyCPU').prop('disabled', true);
    } else if ($('#buyCPU').prop('disabled') && player.money >= player.consumers.miners.price) {
        $('#buyCPU').prop('disabled', false);
    }

    $('#buySuperComputer').html('Buy supercomputer (' + player.consumers.megaMiners.total + ')');
    $('#superComputerCost').text('Cost: $' + suffixAndRound(player.consumers.megaMiners.price, 2));

    if (!$('#buySuperComputer').prop('disabled') && player.money < player.consumers.megaMiners.price) {
        $('#buySuperComputer').prop('disabled', true);
    } else if ($('#buySuperComputer').prop('disabled') && player.money >= player.consumers.megaMiners.price) {
        $('#buySuperComputer').prop('disabled', false);
    }

    $('#buyFactory').html('Buy mining factory (' + player.consumers.miningFactories.total + ')');
    $('#factoryCost').text('Cost: $' + suffixAndRound(player.consumers.miningFactories.price, 2));

    if (!$('#buyFactory').prop('disabled') && player.money < player.consumers.miningFactories.price) {
        $('#buyFactory').prop('disabled', true);
    } else if ($('#buyFactory').prop('disabled') && player.money >= player.consumers.miningFactories.price) {
        $('#buyFactory').prop('disabled', false);
    }

    $('#buySolarPanel').html('Buy solar panel (' + player.generators.solarPanels.total + ')');
    $('#solarCost').text('Cost: $' + suffixAndRound(player.generators.solarPanels.price, 2));

    if (!$('#buySolarPanel').prop('disabled') && player.money < player.generators.solarPanels.price) {
        $('#buySolarPanel').prop('disabled', true);
    } else if ($('#buySolarPanel').prop('disabled') && player.money >= player.generators.solarPanels.price) {
        $('#buySolarPanel').prop('disabled', false);
    }

    $('#buyReactor').html('Buy reactor (' + player.generators.reactors.total + ')');
    $('#reactorCost').text('Cost: $' + suffixAndRound(player.generators.reactors.price, 2));

    if (!$('#buyReactor').prop('disabled') && player.money < player.generators.reactors.price) {
        $('#buyReactor').prop('disabled', true);
    } else if ($('#buyReactor').prop('disabled') && player.money >= player.generators.reactors.price) {
        $('#buyReactor').prop('disabled', false);
    }

    $('#buyBattery').text('Buy battery (' + player.batteries.total + ')');
    $('#batteryCost').text('Cost: $' + suffixAndRound(player.batteries.price, 2));

    if (!$('#buyBattery').prop('disabled') && player.money < player.batteries.price) {
        $('#buyBattery').prop('disabled', true);
    } else if ($('#buyBattery').prop('disabled') && player.money >= player.batteries.price) {
        $('#buyBattery').prop('disabled', false);
    }

    // Upgrades:
    $('#upgrades').children('button').each(function () {
        if (!$(this).prop('disabled') && (player.money < player.upgrades[$(this).data('id')].cost.money || player.coins < player.upgrades[$(this).data('id')].cost.coins)) {
            $(this).prop('disabled', true);
        } else if ($(this).prop('disabled') && player.money >= player.upgrades[$(this).data('id')].cost.money && player.coins >= player.upgrades[$(this).data('id')].cost.coins) {
            $(this).prop('disabled', false);
        }
    });

    // Autoseller:
    $('#coinMarketValue').text('martCoin market value: $' + suffixAndRound(player.coinMarketValue, 2));
    $('#autoSellerTick').text('autoSeller tick: ' + round(player.autoSellerTick, 2) + 'ms');

    // Stock:
    $('#stock').text('Stock: $' + suffixAndRound(player.stockMarket.stock, 2));

    // AI:
    $('#memory').text('Local memory: ' + suffixMemoryAndRound(player.memory, 2) + 'B');
    $('#memoryAI').text('Network memory: ' + suffixMemoryAndRound(player.ai.memory, 2) + 'B / ' + suffixMemoryAndRound(player.ai.availableMemory, 2) + 'B');
    $('#totalThreads').text('Available threads: ' + player.ai.totalFreeThreads + '/' + player.ai.totalThreads);
}

function updateStateElements (player) {
    if (player.state >= 1) {
        $('#autoSellerContainer').show();
        $('#sell').hide();
        $('#coinsPerSecond').css('display', 'inline');
        $('#moneyPerSecond').css('display', 'inline');
    } if (player.state >= 2) {
        $('#toggleAutoMiner').show();
    } if (player.state >= 3) {
        $('#superComputerContainer').show();
    } if (player.state >= 4) {
        $('#stockContainer').show();
    } if (player.state >= 5) {
        $('#AIContainer').show();
        $('#AIResources').show();
    } if (player.state >= 6) {
        $('#reactorContainer').show();
    } if (player.state >= 7) {
        $('#factoryContainer').show();
    }
}

function initializeElements (player) {
    player.updateResourcesPerSecond();
    updateElements(player);
    updateStateElements(player);
    updateTerminal(player);

    // The following are inits that only happen on start up, otherwise the code go in updateElements()

    for (var i = 0; i < player.upgrades.length; ++i) {
        var currentUpgrade = player.upgrades[i];
        if (currentUpgrade.unlockable && !currentUpgrade.unlocked) {
            var cost = '';
            if (currentUpgrade.cost.money > 0) {
                cost += '$' + suffixAndRound(currentUpgrade.cost.money, 2);
            }
            if (currentUpgrade.cost.money > 0 && currentUpgrade.cost.coins > 0) {
                cost += ', ';
            }
            if (currentUpgrade.cost.coins > 0) {
                cost += suffixAndRound(currentUpgrade.cost.coins, 2) + ' martCoins';
            }

            var $input = $('<button class="btn btn-warning btn-block text-left my-3 py-3 px-4">' +
                currentUpgrade.text + ' (' + cost + ')' + '<br>' + '<hr class="my-2">' +
                '<span style="font-weight: 300">' + currentUpgrade.description +'</span>' + '</button>');

            $input.attr('id', 'upgrade' + i);
            $input.data('id', i);
            $input.appendTo($('#upgrades'));
            if (player.money < currentUpgrade.cost.money || player.coins < currentUpgrade.cost.coins) {
                $input.prop('disabled', true);
            }
            $input.click({upgradeIndex: i, player: player}, upgrade);
        }
    }

    for (var i = 0; i < player.ai.threads.length; ++i) {
        $('#thread' + i).text('(' + player.ai.threads[i] + ')');
    }

    if (player.autoSeller) {
        $('#toggleAutoSeller').html('auto_sell.exe running');
        $('#toggleAutoSeller').removeClass('btn-secondary').addClass('btn-success');
    }
    if (player.autoMiner) {
        $('#toggleAutoMiner').html('auto_mine.exe running');
        $('#toggleAutoMiner').removeClass('btn-secondary').addClass('btn-success');
    }

    $('#drift').text('Current drift: ' + player.stockMarket.drift);
    $('#volatility').text('Current volatility: ' + player.stockMarket.volatility);
}

function upgrade (event) {
    var upgrade = event.data.player.upgrades[event.data.upgradeIndex];

    if (event.data.player.upgrade(event.data.upgradeIndex)) {
        $('#upgrade'+event.data.upgradeIndex).remove();
        upgrade.unlocked = true;
        upgrade.doUpgrade(event.data.player);
        updateElements(event.data.player);
        updateStateElements(event.data.player);
    }
}

function updateTerminal (player) {
    var start = player.terminal.history.length - player.terminal.activeLines;
    var colorStart = 0;
    if (start <= -1) {
        start = 0;
        colorStart = player.terminal.activeLines - player.terminal.history.length;
    }

    var terminalLines = '';

    for (var i = start, j = colorStart; i < player.terminal.history.length; ++i, ++j) {
        if (i == player.terminal.history.length - 1) {
            terminalLines += '>: ';
        }
        terminalLines += '<span style="color: ' + player.terminal.colors[j] + '">' + player.terminal.history[i] + '</span><br>';
    }

    $('#terminal').html(terminalLines);
}

function flipCoin () {
    $('#coin').animate({width: 0}, 500, 'easeInQuad', function () {
        $('#coin').animate({width: 100}, 500, 'easeOutQuad', flipCoin);
    });
}