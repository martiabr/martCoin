class Consumer {
    constructor (price, priceMultiplier, total, power, memory, spread) {
        this.price = price;
        this.priceMultiplier = priceMultiplier;
        this.total = total;
        this.power = power;
        this.memory = memory;
        this.spread = spread;
    }
}

class Generator {
    constructor (price, priceMultiplier, total, power) {
        this.price = price;
        this.priceMultiplier = priceMultiplier;
        this.total = total;
        this.power = power;
    }
}

class Battery {
    constructor (price, priceMultiplier, total, capacity) {
        this.price = price;
        this.priceMultiplier = priceMultiplier;
        this.total = total;
        this.capacity = capacity;
    }
}