$(document).ready(function() {
    var SAVE_KEY = 'save';

    var player = new Player();

    initializeElements(player);

    flipCoin();

// Energy generation clock:
    window.setInterval(function () {
        player.generateEnergy();
    }, player.energyTick);

// Upgrade clock:
    window.setInterval(function () {
        for (var i = 0; i < player.upgrades.length; ++i) {
            var currentUpgrade = player.upgrades[i];
            if (!currentUpgrade.unlockable && currentUpgrade.checkUnlock(player)) {
                currentUpgrade.unlockable = true;
                var cost = '';
                if (currentUpgrade.cost.money > 0) {
                    cost += '$' + suffixAndRound(currentUpgrade.cost.money, 2);
                }
                if (currentUpgrade.cost.money > 0 && currentUpgrade.cost.coins > 0) {
                    cost += ', ';
                }
                if (currentUpgrade.cost.coins > 0) {
                    cost += suffixAndRound(currentUpgrade.cost.coins, 2) + ' martCoins';
                }

                var $input = $('<button class="btn btn-warning btn-block text-left my-3 py-3 px-4">' +
                    currentUpgrade.text + ' (' + cost + ')' + '<br>' + '<hr class="my-2">' +
                    '<span style="font-weight: 300">' + currentUpgrade.description +'</span>' + '</button>');

                $input.attr('id', 'upgrade' + i);
                $input.data('id', i);
                $input.appendTo($('#upgrades'));
                if (player.money < currentUpgrade.cost.money || player.coins < currentUpgrade.cost.coins) {
                    $input.prop('disabled', true);
                }
                $input.click({upgradeIndex: i, player: player}, upgrade);
            }
        }
    }, player.upgradeTick);

// Stock market clock:
    window.setInterval(function () {
        player.stockMarket.updateStock(player.stockTick);

        // Canvas drawing:
        var canvas = document.getElementById('stockCanvas');
        var context = canvas.getContext('2d');
        canvas.width = 800;
        canvas.height = 400;
        var border = 25;

        var end = player.stockMarket.data.length - 1;
        var start = player.stockMarket.data.length - player.stockMarket.dataPoints - 1;
        if (start < 0) {
            start = 0;
        }

        var data = player.stockMarket.data.slice(start, end + 1);
        var min = Math.min(...data);
        var max = Math.max(...data);
        var difference = max - min;
        var steps = player.stockMarket.dataPoints;

        context.clearRect(0, 0, canvas.width, canvas.height);

        context.strokeStyle = "#404040";
        context.lineWidth = 1;
        context.beginPath();
        for (var i = player.stockMarket.dataPoints - player.stockMarket.currentTick; i >= 0; i -= player.stockMarket.dataPoints / player.stockMarket.lines) {
            context.moveTo(i * canvas.width / player.stockMarket.dataPoints, 0);
            context.lineTo(i * canvas.width / player.stockMarket.dataPoints, canvas.height);
        }
        context.stroke();

        context.strokeStyle = "yellow";
        context.lineWidth = 4;
        context.beginPath();
        context.moveTo(canvas.width, (canvas.height - border * 2) * (1 - (player.stockMarket.data[end] - min) / difference) + border);

        for (var i = end, j = steps; i > start; --i, --j) {
            context.lineTo((j - 1) / steps * canvas.width, (canvas.height - border * 2) * (1 - (player.stockMarket.data[i - 1] - min) / difference) + border);
        }
        context.stroke();

    }, player.stockTick);


// AI clock:
    window.setInterval(function () {
        player.aiUpdate();
    }, player.aiTick);

    window.setInterval(function () {
        player.updateResourcesPerSecond();
        updateElements(player);
    }, 100);


// Button event handlers: |--------------------------------------------------------------------|

    var miningInterval;

    $('#mineBlock').click(function () {
        if (!player.miningInProgress) {
            // Initialize new block:
            $('#mineBlock').prop("disabled", true);
            $('#miningProgress').text('');
            player.currentSequence = '';
            player.zeroes = 0;
            var ticks = 0;
            player.miningInProgress = true;

            // Initial hash:
            $('#hash').text(player.updateSequence());

            // Update sequence until difficulty level is meet:
            miningInterval = window.setInterval(function () {
                ticks++;
                if (player.useEnergy()) {
                    $('#hash').text(player.updateSequence());

                    if (player.zeroes >= player.difficulty) {
                        $('#mineBlock').prop("disabled", false);
                        window.clearInterval(miningInterval);

                        player.terminal.history.push('Node finished, mined ' + suffixAndRound(player.completeBlock(), 2) + ' martCoin in ' + ticks + ' ticks');
                        updateTerminal(player);

                        $('#coins').text('MartCoins: ' + suffixAndRound(player.coins, 2));
                    }
                }
            }, player.miningTick);
        }
    });

    $('#toggleAutoMiner').click(function () {
        if (player.toggleAutoMiner()) {
            $('#mineBlock').click();
            $(this).html('auto_mine.exe running');
            $(this).removeClass('btn-secondary').addClass('btn-success');
        } else {
            $(this).html('auto_mine.exe off');
            $(this).removeClass('btn-success').addClass('btn-secondary');
        }
    });

    $('#buyCPU').click(function () {
        if (player.buyItem(player.consumers.miners)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buyCPU').html('Buy CPU (' + player.consumers.miners.total + ')');
            $('#CPUCost').text('Cost: $' + suffixAndRound(player.consumers.miners.price, 2));
        }
    });

    $('#buySuperComputer').click(function () {
        if (player.buyItem(player.consumers.megaMiners)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buySuperComputer').html('Buy supercomputer (' + player.consumers.megaMiners.total + ')');
            $('#superComputerCost').text('Cost: $' + suffixAndRound(player.consumers.megaMiners.price, 2));
        }
    });

    $('#buyFactory').click(function () {
        if (player.buyItem(player.consumers.miningFactories)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buyFactory').html('Buy mining factory (' + player.consumers.miningFactories.total + ')');
            $('#factoryCost').text('Cost: $' + round(player.consumers.miningFactories.price, 2));
        }
    });

    $('#buySolarPanel').click(function () {
        if (player.buyItem(player.generators.solarPanels)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buySolarPanel').html('Buy solar panel (' + player.generators.solarPanels.total + ')');
            $('#solarCost').text('Cost: $' + suffixAndRound(player.generators.solarPanels.price, 2));
        }
    });

    $('#buyReactor').click(function () {
        if (player.buyItem(player.generators.reactors)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buyReactor').html('Buy reactor (' + player.generators.reactors.total + ')');
            $('#reactorCost').text('Cost: $' + suffixAndRound(player.generators.reactors.price, 2));
        }
    });

    $('#buyBattery').click(function () {
        if (player.buyItem(player.batteries)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#buyBattery').text('Buy battery (' + player.batteries.total + ')');
            $('#batteryCost').text('Cost: $' + suffixAndRound(player.batteries.price, 2));
        }
    });

    $('#sell').click(function () {
        if (player.sellCoin(1)) {
            $('#money').text('Money: $' + suffixAndRound(player.money, 2));
            $('#coins').text('MartCoins: ' + suffixAndRound(player.coins, 2));
        }
    });

    var autoSellerInterval;

    $('#toggleAutoSeller').click(function () {
        if (player.toggleAutoSeller()) {
            autoSellerInterval = window.setInterval(function () {
                $('#sell').click();
            }, player.autoSellerTick);
            $(this).html('auto_sell.exe running');
            $(this).removeClass('btn-secondary').addClass('btn-success');
        } else {
            window.clearInterval(autoSellerInterval);
            $(this).html('auto_sell.exe off');
            $(this).removeClass('btn-success').addClass('btn-secondary');
        }
    });

    $('#decreaseCoinMarketValue').click(function () {
        player.changeCoinMarketValue(false);
        if (player.autoSeller) {
            window.clearInterval(autoSellerInterval);
            autoSellerInterval = window.setInterval(function () {
                $('#sell').click();
            }, player.autoSellerTick);
        }

        $('#coinMarketValue').text('martCoin market value: $' + suffixAndRound(player.coinMarketValue, 2));
        $('#autoSellerTick').text('autoSeller tick: ' + round(player.autoSellerTick, 2) + 'ms');
    });

    $('#increaseCoinMarketValue').click(function () {
        player.changeCoinMarketValue(true);
        if (player.autoSeller) {
            window.clearInterval(autoSellerInterval);
            autoSellerInterval = window.setInterval(function () {
                $('#sell').click();
            }, player.autoSellerTick);
        }

        $('#coinMarketValue').text('martCoin market value: $' + suffixAndRound(player.coinMarketValue, 2));
        $('#autoSellerTick').text('autoSeller tick: ' + round(player.autoSellerTick, 2) + 'ms');

    });

    $('#buyStockInput').on('input', function () {
        if ($(this).val() > player.money) {
            $(this).val(player.money);
        } else if ($(this).val() < 0) {
            $(this).val(0);
        }
    });

    $('#buyStockMax').click(function () {
        player.invest(player.money);
        $('#stock').text('Stock: $' + suffixAndRound(player.stockMarket.stock, 2));
        $('#money').text('Money: $' + suffixAndRound(player.money, 2));
    });

    $('#buyStock').click(function () {
        player.invest(Number($('#buyStockInput').val()));
        $('#stock').text('Stock: $' + suffixAndRound(player.stockMarket.stock, 2));
        $('#money').text('Money: $' + suffixAndRound(player.money, 2));
    });

    $('#sellStockInput').on('input', function () {
        if ($(this).val() > player.stockMarket.stock) {
            $(this).val(player.stockMarket.stock);
        } else if ($(this).val() < 0) {
            $(this).val(0);
        }
    });

    $('#sellStockMax').click(function () {
        player.invest(-player.stockMarket.stock);
        $('#stock').text('Stock: $' + suffixAndRound(player.stockMarket.stock, 2));
        $('#money').text('Money: $' + suffixAndRound(player.money, 2));
    });

    $('#sellStock').click(function () {
        player.invest(-Number($('#sellStockInput').val()));
        $('#stock').text('Stock: $' + suffixAndRound(player.stockMarket.stock, 2));
        $('#money').text('Money: $' + suffixAndRound(player.money, 2));
    });

    for (var i = 0; i < player.ai.threads.length; ++i) {
        $('#removeThread' + i).click(function () {
            var id = $(this).data('id');
            player.ai.removeThread(id);
            $('#thread' + id).text('(' + player.ai.threads[id] + ')');

            if (id == 3 && player.ai.threads[3] == 0) {
                window.clearInterval(miningInterval);
                $('#mineBlock').prop("disabled", false);
                player.miningInProgress = false;

                if (player.autoMiner) {
                    player.autoMiner = false;
                }
            }
        });

        $('#addThread' + i).click( function () {
            var id = $(this).data('id');
            player.ai.addThread(id);
            $('#thread' + id).text('(' + player.ai.threads[id] + ')');
        });
    }

    $('#save').click(function () {
        player.miningInProgress = false;
        save(player, SAVE_KEY);
        console.log("Saved game");
    });

    $('#load').click(function () {
        var loadedPlayer = load(SAVE_KEY);

        if (loadedPlayer !== null) {
            $.extend(player, loadedPlayer);

            player.stockMarket = new StockMarket();
            $.extend(player.stockMarket, loadedPlayer.stockMarket);

            player.ai = new AI();
            $.extend(player.ai, loadedPlayer.ai);

            player.constructUpgrades();

            for (var i = 0; i < player.upgrades.length; ++i) {
                $.extend(player.upgrades[i], loadedPlayer.upgrades[i]);
            }

            initializeElements(player);
            console.log("Loaded game from local storage");
            console.log(player);
        }
    });
});