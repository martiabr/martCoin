// The player class is basically the game controller. This class handles all the logic, no interfacing.
class Player {
    constructor () {
        var player = this;

        // Resources:
        this.money = 0;
        this.moneyEarned = 0;
        this.coins = 0;
        this.coinsEarned = 0;
        this.energy = 50000;
        this.researchPoints = 0;
        this.state = 0;


        // Terminal:
        this.terminal = {
            history: [],
            activeLines: 4,
            colors: ['#43810B', '#67B613', '#92E230', '#A5FF36'],
            loading: ['|', '/', '--', '\\']
        };


        // Energy consumers:
        this.consumers = {};
        this.consumers.miners = new Consumer(20, 1.3, 1, 400, 3, 2);
        this.consumers.megaMiners = new Consumer(100, 1.25, 0, 10000, 50, 30);
        this.consumers.miningFactories = new Consumer(2000, 1.3, 0, 300000, 650, 300);


        // Upgrades:
        this.upgradeTick = 100;

        this.constructUpgrades();


        // Mining data:
        this.miningTick = 200;
        this.hashLength = 64;
        this.numberWeight = 0.5;
        this.zeroWeight = 0.2;
        this.changeWeight = 0.5;
        this.difficulty = 4;
        this.difficultyBonus = 1;
        this.zeroes = 0;
        this.currentSequence = '';
        this.miningInProgress = false;
        this.autoMiner = false;
        this.memory = this.consumers.miners.memory;


        // Sell data:
        this.autoSeller = false;
        this.coinMarketValue = 8;
        this.coinMarketValueMultiplier = 1.5;
        this.coinMarketValueMin = 0.01;
        this.coinMarketValueMax = 1000;
        this.autoSellerTick = 1000;
        this.autoSellerTickMultiplier = 2;
        this.moneyPerSecond = 0;
        this.coinsPerSecond = 0;


        // Energy generators:
        this.generators = {};
        this.generators.solarPanels = new Generator(10, 1.17, 1, 180);
        this.generators.reactors = new Generator(10000, 1.17, 0, 10000);

        this.energyTick = 100;
        this.energyPerSecond = 0;

        this.batteries = new Battery(100, 1.5, 1, 50000);

        // Stock:
        this.stockMarket = new StockMarket(100);
        this.stockTick = 1000;

        //AI module:
        this.ai = new AI();
        this.availableMemoryMultiplier = 0.001;
        this.memory = 0;
        this.memoryMultiplier = 0.0005;
        this.researchPointMultiplier = 0.00015;
        this.aiTick = 100;
        this.RPsPerSecond = 0;
    }

    constructUpgrades () {
        this.upgrades = [];

        this.upgrades.push(new CPUUpgrade(100, 0, "Upgrade CPU memory", '+100% CPU memory, +50% CPU power consumption', null, 5, 1.5, 2));

        this.upgrades.push(new StateUpgrade(500, 0, "Install autoSeller script", 'Install script that automatically sells coins', null, 150));

        this.upgrades.push(new MarketUpgrade(0, 100, "Increase martCoin market value", 'autoSeller coin value +25%', null, 200));

        this.upgrades.push(new StateUpgrade(1000, 0, "Install autoMiner script", 'Gain access to the automatic miner module', 1, 350));

        this.upgrades.push(new CPUUpgrade(2500, 0, "Boost CPU clocks", '+100% CPU memory, +50% CPU power consumption', 0, 15, 1.6, 1.4));

        this.upgrades.push(new SolarPanelUpgrade(2000, 0, "Upgrade solar panels", '+50% solar panel power generation', null, 500));

        this.upgrades.push(new MiningSpeedUpgrade(3000, 0, "Optimize mining algorithm", '+20% mining speed', null, 700));

        this.upgrades.push(new MarketUpgrade(0, 350, "Increase martCoin market value II", 'autoSeller coin value +25%', 2, 850));

        this.upgrades.push(new DifficultyUpgrade(2500, 200, "Increase mining algorithm difficulty", '+2 proof difficulty, +100% mining reward', 6, 1000, 2));

        this.upgrades.push(new CPUUpgrade(10000, 0, "Upgrade CPU memory II", '+100% CPU memory, +50% CPU power consumption', 4, 25));

        this.upgrades.push(new StateUpgrade(5000, 0, "Unlock superComputers", 'Gain access to superComputers with base memory 50B', 3, 1100));

        this.upgrades.push(new StateUpgrade(0, 0, "Unlock stock trading", 'Gain access to stock trader module', null, 1100));

        this.upgrades.push(new StateUpgrade(0, 0, "Install AI network", 'Install AI script that does research for you', null, 1100));

        this.upgrades.push(new StateUpgrade(0, 0, "Unlock reactors", 'Gain access to reactors with +100kW', null, 1100));

        this.upgrades.push(new StateUpgrade(0, 0, "Unlock mining factories", 'Gain access to factories with base memory 650B', null, 1100));
    }

    updateSequence () {
        var sequence = '0'.repeat(this.zeroes);
        var letters = 'abcdefghijklmnopqrstuvwxyz';
        var numbers = '123456789';

        for (var i = this.zeroes; i < this.hashLength; ++i) {
            if (Math.random() < this.numberWeight) {
                if (i === this.zeroes && Math.random() < this.zeroWeight && this.currentSequence !== '') {
                    sequence += '0';
                    this.zeroes ++;
                } else if (Math.random() < this.changeWeight || this.currentSequence === '') {
                    sequence += numbers.charAt(Math.floor(Math.random()*numbers.length));
                } else {
                    sequence += this.currentSequence.charAt(i);
                }
            } else {
                if (Math.random() < this.changeWeight || this.currentSequence === '') {
                    sequence += letters.charAt(Math.floor(Math.random()*letters.length));
                } else {
                    sequence += this.currentSequence.charAt(i);
                }
            }
        }

        this.currentSequence = sequence;

        return sequence;
    }

    useEnergy () {
        var energy = this.energy;
        for (var item in this.consumers) {
            item = this.consumers[item];
            energy -= item.total * item.power;
        }
        if (energy >= 0) {
            this.energy = energy;
            return true;
        } return false;
    }

    generateEnergy () {
        for (var item in this.generators) {
            item = this.generators[item];
            this.energy += item.total * item.power;
        }
        if (this.energy > this.batteries.total * this.batteries.capacity) {
            this.energy = this.batteries.total * this.batteries.capacity;
        }
    }

    completeBlock () {
        var deltaCoins = 0;

        for (var item in this.consumers) {
            item = this.consumers[item];
            deltaCoins += (Math.random() * item.spread * 2
                + item.memory - item.spread) * item.total;
        }

        deltaCoins += this.ai.memory;

        deltaCoins = deltaCoins * this.difficultyBonus * this.ai.threads[3];

        this.coins += deltaCoins;
        this.coinsEarned += deltaCoins;
        this.miningInProgress = false;

        if (this.autoMiner) {
            $('#mineBlock').click();
        }

        return deltaCoins;
    }

    buyItem (item) {
        if (this.money >= item.price) {
            item.total ++;
            this.money -= item.price;
            item.price *= item.priceMultiplier;
            return true;
        }
        return false;
    }

    sellCoin (quantity) {
        if (this.coins >= quantity) {
            this.coins -= quantity;
            this.money += quantity * this.coinMarketValue;
            this.moneyEarned += quantity * this.coinMarketValue;
            return true;
        } return false;
    }

    invest (money) {
        if (money <= this.money && this.stockMarket.stock + money >= 0) {
            this.money -= money;
            this.stockMarket.invest(money);
            return true;
        } return false;
    }

    toggleAutoMiner () {
        this.autoMiner = !this.autoMiner;
        return this.autoMiner;
    }

    toggleAutoSeller () {
        this.autoSeller = !this.autoSeller;
        return this.autoSeller;
    }

    changeCoinMarketValue (increase) {
        if (increase && this.coinMarketValue * this.coinMarketValueMultiplier < this.coinMarketValueMax) {
            this.coinMarketValue *= this.coinMarketValueMultiplier;
            this.autoSellerTick *= this.autoSellerTickMultiplier;
            return true;
        } else if (!increase && this.coinMarketValue / this.coinMarketValueMultiplier > this.coinMarketValueMin) {
            this.coinMarketValue /= this.coinMarketValueMultiplier;
            this.autoSellerTick /= this.autoSellerTickMultiplier;
            return true;
        }
        return false;
    }

    upgrade (id) {
        if (id >= 0 && id < this.upgrades.length) {
            var upgrade = this.upgrades[id];
            if (upgrade.unlockable === true && this.money - upgrade.cost.money >= 0 && this.coins - upgrade.cost.coins >= 0) {
                upgrade.unlocked = true;
                this.money -= upgrade.cost.money;
                this.coins -= upgrade.cost.coins;
                return true;
            } return false;
        } return false;
    }

    nextState () {
        this.state ++;
    }

    aiUpdate () {
        var memory = 0;
        for (var item in this.consumers) {
            item = this.consumers[item];
            memory += item.memory * item.total;
        }
        this.memory = memory;

        var totalMemory = memory + this.ai.memory;

        this.researchPoints += this.ai.threads[0] * this.researchPointMultiplier * totalMemory;
        this.ai.availableMemory += this.ai.threads[1] * this.availableMemoryMultiplier * totalMemory;

        var newMemory = this.ai.memory + this.ai.threads[2] * this.memoryMultiplier * totalMemory;
        if (newMemory < this.ai.availableMemory) {
            this.ai.memory = newMemory;
        }
    }

    updateResourcesPerSecond () {
        this.moneyPerSecond = this.coinMarketValue / this.autoSellerTick * 1000 * this.autoSeller;

        var energy = 0;
        if (this.miningInProgress) {
            for (var item in this.consumers) {
                item = this.consumers[item];
                energy -= item.total * item.power / this.miningTick * 1000;
            }
        }
        for (var item in this.generators) {
            item = this.generators[item];
            energy += item.total * item.power / this.energyTick * 1000;
        }
        this.energyPerSecond = energy;

        var chance = this.numberWeight * this.zeroWeight;
        var hitsPerSecond = chance / this.miningTick * 1000;
        var blocksPerSecond = hitsPerSecond / this.difficulty;

        var coins = 0;
        for (var item in this.consumers) {
            item = this.consumers[item];
            coins += item.memory * item.total;
        }

        coins += this.ai.memory;

        this.coinsPerSecond = blocksPerSecond * this.difficultyBonus * this.ai.threads[3] * coins * this.miningInProgress - 1000 / this.autoSellerTick * this.autoSeller;

        this.RPsPerSecond = this.ai.threads[0] * this.researchPointMultiplier * (this.memory + this.ai.memory) / this.aiTick * 1000;
    }
}