class StockMarket {
    constructor (stock) {
        this.stock = stock;
        this.drift = 0.000001; // how fast the stock rises
        this.volatility = 0.05; // how big "the jumps" are
        this.data = [stock];
        this.dataPoints = 64;
        this.lines = 8;
        this.currentTick = 0;
    }

    updateStock (delta_t) {
        this.currentTick = (this.currentTick + 1) % (this.dataPoints / this.lines);

        // Box Muller transformation to get random standard normal distributed number z:
        var x = Math.random();
        var y = Math.random();
        var z = Math.sqrt(-2 * Math.log(x)) * Math.cos(2 * Math.PI * y);

        this.stock += this.stock * (this.drift * delta_t + this.volatility * z);

        this.data.push(this.stock);

        return this.stock;
    }

    invest (money) {
        this.stock += money;
    }
}