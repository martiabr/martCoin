class Upgrade {
    constructor (money, coins, text, description, prerequisite, total) {
        this.text = text;
        this.description = description;
        this.total = total;
        this.unlockable = false;
        this.unlocked = false;
        this.cost = {
            money: money,
            coins: coins
        };
        this.prerequisite = prerequisite;
    }

    checkUnlock (player) {
        return player.coinsEarned >= this.total && (this.prerequisite === null || player.upgrades[this.prerequisite].unlocked);
    }
}

class CPUUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total, power, reward) {
        super(money, coins, text, description, prerequisite, total);
        this.powerMultiplier = power;
        this.rewardMultiplier = reward;
    }

    checkUnlock (player) {
        return player.consumers.miners.total >= this.total && (this.prerequisite === null || player.upgrades[this.prerequisite].unlocked);
    }

    doUpgrade (player) {
        player.consumers.miners.power *= this.powerMultiplier;
        player.consumers.miners.rewardLower *= this.rewardMultiplier;
        player.consumers.miners.rewardUpper *= this.rewardMultiplier;
    }
}

class StateUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total) {
        super(money, coins, text, description, prerequisite, total);
    }

    doUpgrade (player) {
        player.nextState();
    }
}

class MarketUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total) {
        super(money, coins, text, description, prerequisite, total);
        this.coinValueMultiplier = 1.25;
    }

    doUpgrade (player) {
        player.coinMarketValue *= this.coinValueMultiplier;
    }
}

class SolarPanelUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total) {
        super(money, coins, text, description, prerequisite, total);
        this.powerMultiplier = 1.5;
    }

    checkUnlock (player) {
        return player.generators.solarPanels.total >= this.total && (this.prerequisite === null || player.upgrades[this.prerequisite].unlocked);
    }

    doUpgrade (player) {
        player.generators.solarPanels.power *= this.powerMultiplier;
    }
}

class MiningSpeedUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total) {
        super(money, coins, text, description, prerequisite, total);
        this.miningSpeedMultiplier = 0.8;
    }

    doUpgrade (player) {
        player.miningTick *= this.miningSpeedMultiplier;
    }
}

class DifficultyUpgrade extends Upgrade {
    constructor (money, coins, text, description, prerequisite, total, difficultyIncrease) {
        super(money, coins, text, description, prerequisite, total);
        this.difficultyIncrease = difficultyIncrease;
        this.difficultyBonus = 2;
    }

    doUpgrade (player) {
        player.difficulty += this.difficultyIncrease;
        player.difficultyBonus += this.difficultyBonus;
    }
}