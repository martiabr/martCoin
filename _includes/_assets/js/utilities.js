function round(value, decimals) {
    return (Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals)).toFixed(decimals);
}

function suffixAndRound(value, decimals) {
    var suffixes = ['K', 'M', 'B', 'T'];
    var length = Math.floor(value).toString().length;

    for (var i = suffixes.length - 1; i >= 0; --i) {
        if (length > (i + 1) * 3) {
            return round(value / Math.pow(10, (i + 1) * 3), decimals) + suffixes[i];
        }
    }

    return round(value, decimals);
}

function suffixMemoryAndRound(value, decimals) {
    var suffixes = ['K', 'M', 'G', 'T'];
    var length = Math.floor(value).toString().length;

    for (var i = suffixes.length - 1; i >= 0; --i) {
        if (length > (i + 1) * 3) {
            return round(value / Math.pow(10, (i + 1) * 3), decimals) + suffixes[i];
        }
    }

    return round(value, decimals);
}

function save(player, saveKey) {
    localStorage.setItem(saveKey, JSON.stringify(player));
}

function load(saveKey) {
    return JSON.parse(localStorage.getItem(saveKey));
}